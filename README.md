Adventure WV 2020
==================

**Description:** Adventure WV 2020.

**Developers name(s):** Tatsu Johnson

**Github repository URL:** [https://bitbucket.org/wvudigital/adventurewv-2020/src/master/]

**Documentation website URL:** [https://designsystem.wvu.edu](https://designsystem.wvu.edu)

**Dependencies necessary to work with this theme:** Sass.

## Adventure WV 2020 & Gulp

**Requirements**

  * [NodeJS](https://nodejs.org)

You will need to install Node ^12.13.1.

  1. Download and install NodeJS from https://nodejs.org/en if you haven't already.
  1. Install Gulp globally by entering `npm install -g gulp` in your terminal.
  1. Navigate to your project's directory via terminal (something like `cd ~/Sites/cleanslate_themes/MY-SITE`)
  1. Install node modules by typing `npm ci`
  1. Run Gulp by typing `gulp`.

**Note:** the `gulpfile.js` in its base form will only compile your Sass.


